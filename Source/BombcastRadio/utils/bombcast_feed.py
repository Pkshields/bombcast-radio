﻿from bombcastradio import config

import feedparser
from time import mktime
from datetime import datetime

def parse_bombcast_feed():
    raw_bombcast_feed = feedparser.parse(config.BOMBCAST_FEED)
    bombcast_list = []

    for raw_bombcast in raw_bombcast_feed["entries"]:
        # Parse the Python struct_time pbject to a Django happy datetime object
        pub_date = datetime.fromtimestamp(mktime(raw_bombcast["published_parsed"]))

        bombcast = { "title": raw_bombcast["title"],
                     "url": raw_bombcast["media_content"][0]["url"],
                     "description": raw_bombcast["summary"],
                     "img_url": raw_bombcast["image"]["href"],
                     "pub_date": pub_date, 
                     "duration": raw_bombcast["itunes_duration"] }

        bombcast_list.insert(0, bombcast)

    return bombcast_list