﻿from datetime import datetime, timedelta
from django.utils.timezone import utc
 
class WeekdayEnum():
    MONDAY = 0;
    TUESDAY = 1;
    WEDNESDAY = 2;
    THURSDAY = 3;
    FRIDAY = 4;
    SATURDAY = 5;
    SUNDAY = 6;


def add_secs(dt, secs_to_add): 
    return dt + timedelta(0, secs_to_add)

def subtract_secs(dt, secs_to_add): 
    return dt - timedelta(0, secs_to_add)

def no_timezone(dt):
    return dt.replace(tzinfo=None)

def utcnow():
    return datetime.utcnow().replace(tzinfo=utc)

def to_string(dt):
    return dt.strftime("%Y-%m-%d %H:%M:%S")

def weekday():
    return datetime.today().weekday()