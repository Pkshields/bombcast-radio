import json
import datetime
from time import mktime

class DateTimeEncoder(json.JSONEncoder):
    """
        JSON Encoder that encodes datetime objects as a normal UNIX timestamp

        Usage:
        print json.dumps(obj, cls = DateTimeEncoder)
    """

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return int(mktime(obj.timetuple()))

        return json.JSONEncoder.default(self, obj)
