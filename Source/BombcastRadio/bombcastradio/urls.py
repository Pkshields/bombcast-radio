﻿"""
Definition of urls for BombcastRadio.
"""

from bombcastradio import settings

from django.conf.urls import patterns, include, url

# Admin
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # API
    url(r'^api/radioplaylist/$', 'apps.api.views.api.get_radio_playlist', name='get_radio_playlist'),
    url(r'^api/radioplaylist/(?P<count>[0-9])/$', 'apps.api.views.api.get_radio_playlist', name='get_radio_playlist'),

    # Website
    url(r'^$', 'apps.website.views.index', name='main_website_page'),

    # Admin
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

# Enable the Cron URLs if debugging is enabled
if settings.DEBUG:
    urlpatterns += patterns('',    
        url(r'^cron/updatedb/$', 'apps.api.views.update.update_db', name='update_db'),
        url(r'^cron/updateradioplaylist/$', 'apps.api.views.update.update_radio_playlist', name='update_radio_playlist'),
    )