Django==1.8.3
django-braces==1.8.1
feedparser==5.2.1
mysqlclient==1.3.6
six==1.9.0
wheel==0.24.0
