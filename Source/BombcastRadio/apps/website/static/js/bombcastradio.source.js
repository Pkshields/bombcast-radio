﻿var timerHTMLObject;
var timer;
var timerOffset = 0;

function setTimerObject(object) {
    timerHTMLObject = object;
}

function startTimer(pausedTimeFunction) {
    timer = setInterval(function () {
        var pausedTime = parseInt(stringToFunctionCall(pausedTimeFunction));
        timerOffset++;
        timerHTMLObject.text(pausedTime + timerOffset);
    }, 1000);
}

function stopTimer() {
    clearInterval(timer);
    timerOffset = 0;
}

function jQueryAddClassOnce(jQueryObject, classToAdd) {
    jQueryObject.removeClass(classToAdd);
    jQueryObject.addClass(classToAdd);
}

function stringToFunctionCall(functionName) {
    return window[functionName]();
}