﻿/* 
 * Taken from HTML file:
 * 
 * soundmanagerSWFLocations
 */

var LENGTHOFTIMETOREWIND = 5;

var currentPlayingBombcast;
var timeRewound = 0;
var timePlayerWasPaused = Date.now();
var firstPlay = true;

soundManager.setup({
    url: soundmanagerSWFLocations,
    flashVersion: 9, // optional: shiny features (default = 8)
    preferFlash: false,
    onready: function () {
        currentPlayingBombcast = createSound(startBombcast);
        currentPlayingBombcast.setPosition(startPosition);
        currentPlayingBombcast.play();
        currentPlayingBombcast.pause();
    }
});

function createSound(url) {
    return soundManager.createSound({
        id: 'currentPlayingBombcast',
        url: url,
        onfinish: function () {
            playNextBombcast();
        }
    });
}

function playNextBombcast() {
    $.ajax({
        type: 'GET',
        url: 'api/radioplaylist',
        dataType: 'json',
        success: function (data) {
            // Start playing new bombcast
            currentPlayingBombcast.destruct();
            currentPlayingBombcast = createSound(data[0].url);
            currentPlayingBombcast.play();

            // Update playing bombcast data on the page
            $("#currentbombcast #imgcontainer img").attr("src", data[0].img_url);
            $("#currentbombcast #textcontainer h3").html(data[0].title);
            $("#currentbombcast #textcontainer p").html(data[0].description);
        }
    });
}

/**************************************************************/

function pausePlayer() {
    if (currentPlayingBombcast.paused) {
        currentPlayingBombcast.play();

        if (firstPlay) {
            resetPlayer();
            firstPlay = false;
        }
        else {
            timeRewound += Date.now() - timePlayerWasPaused;
            timePlayerWasPaused = 0;
        }
    }
    else {
        currentPlayingBombcast.pause();

        timePlayerWasPaused = Date.now();
    }
}

function rewindPlayer() {
    var possibleTime = currentPlayingBombcast.position - toMilliseconds(LENGTHOFTIMETOREWIND);
    if (possibleTime >= 0)
        currentPlayingBombcast.setPosition(possibleTime);
    else
        currentPlayingBombcast.currentTime = 0;

    timeRewound += toMilliseconds(LENGTHOFTIMETOREWIND);
}

function resetPlayer() {
    if (timeRewound != 0) {
        var newTime = currentPlayingBombcast.position + timeRewound;
        currentPlayingBombcast.setPosition(newTime);
        timeRewound = 0;
    }
}

/**************************************************************/

function playerIsPaused() {
    return currentPlayingBombcast.paused;
}

function playerGetCurrentTimePaused() {
    return timeRewound / 1000;
}

/**************************************************************/

function toMilliseconds(time) {
    return time * 1000;
}