﻿from apps.api.internal import api

from django.shortcuts import render_to_response

def index(request):
    radio_playlist = api.get_radio_playlist()
    return render_to_response("index.html", {'radio_playlist' : radio_playlist})