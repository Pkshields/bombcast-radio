﻿from apps.api.models import RadioPlaylist
from bombcastradio import config

from django.http import HttpResponse

def get_radio_playlist(count = config.RADIO_PLAYLIST_API_DEFAULT_NUM_TO_DISPLAY):
    return RadioPlaylist.objects.get_public_playlist()
