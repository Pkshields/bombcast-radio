﻿from utils import datetime_helper
from utils.datetime_encoder import DateTimeEncoder

from django.db import models
from datetime import datetime
import json

class RadioPlaylistManager(models.Manager):

    def get_last_bombcast(self):
        ordered_bombcasts = self.order_by('start_time')
        
        if len(ordered_bombcasts) == 0:
            return None
        else:
            # Last element in database. Negative indexing is not supported on QuerySets apparently
            return ordered_bombcasts.reverse()[0]


    def add_to_database(self, bombcast_list):
        for bombcast in bombcast_list:
            b = self.create(bombcast=bombcast["bombcast"],
                            start_time=bombcast["start_time"])


    def clean_played_bombcasts(self):
        cutoff_date = datetime_helper.subtract_secs(datetime_helper.utcnow(), 24*60*60)
        played_bombcasts = self.filter(start_time__lte=cutoff_date)  # _lte is same as <=
        played_bombcasts.delete()


    def get_public_playlist(self, count = 10):
        public_bombcast_data = []

        # Find the start date of the bombcast that is currently playing
        # in the radio playlist, the use it's start time to filter the QuerySet
        first_bombcast_in_playlist_start_time = datetime_helper.utcnow()
        for bombcast in self.all():
            if bombcast.end_time() >= first_bombcast_in_playlist_start_time:
                first_bombcast_in_playlist_start_time = bombcast.start_time
                break

        current_playlist = self.filter(start_time__gte=first_bombcast_in_playlist_start_time)

        for playlist_bombcast in current_playlist:
            single_public_bombcast_data = { "title": playlist_bombcast.bombcast.title,
                                            "url": playlist_bombcast.bombcast.url,
                                            "description": playlist_bombcast.bombcast.description,
                                            "img_url": playlist_bombcast.bombcast.img_url,
                                            "duration": playlist_bombcast.bombcast.duration,
                                            "start_time": playlist_bombcast.start_time }
            public_bombcast_data.append(single_public_bombcast_data)

        # Note: Only returning the 1st X bombcasts in the playlist (Default 10)
        return public_bombcast_data[0:count]


    def serialize_playlist(self, count):
        public_bombcast_data = self.get_public_playlist(count)

        return json.dumps(public_bombcast_data, cls = DateTimeEncoder)