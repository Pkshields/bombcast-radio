﻿from django.db import models

class BombcastsManager(models.Manager):

    def get_last_bombcast(self):
        ordered_bombcasts = self.order_by('pub_date')
        
        if len(ordered_bombcasts) == 0:
            return None
        else:
            # Last element in database. Negative indexing is not supported on QuerySets apparently
            return ordered_bombcasts.reverse()[0]

    def add_to_database(self, bombcast_list):
        for bombcast in bombcast_list:
            
            if (bombcast["duration"] == ""):
                bombcast["duration"] = 0

            b = self.create(title=bombcast["title"],
                            description=bombcast["description"],
                            url=bombcast["url"],
                            img_url=bombcast["img_url"],
                            pub_date=bombcast["pub_date"],
                            duration=bombcast["duration"])

    def get_bombcasts_in_random_order(self):
        return self.order_by('?')