# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bombcasts',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=60)),
                ('description', models.TextField()),
                ('url', models.CharField(max_length=120)),
                ('img_url', models.CharField(max_length=100)),
                ('pub_date', models.DateTimeField()),
                ('duration', models.IntegerField()),
            ],
            options={
                'verbose_name_plural': 'Bombcasts',
            },
        ),
        migrations.CreateModel(
            name='RadioPlaylist',
            fields=[
                ('start_time', models.DateTimeField(serialize=False, primary_key=True)),
                ('bombcast', models.ForeignKey(to='api.Bombcasts')),
            ],
            options={
                'verbose_name_plural': 'Radio Playlist',
                'ordering': ['start_time'],
            },
        ),
    ]
