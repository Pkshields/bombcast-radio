from django.contrib import admin

from apps.api.models import Bombcasts, RadioPlaylist 

class BombcastsFilters(admin.SimpleListFilter):
    title = "Duration"
    parameter_name = "duration"

    def lookups(self, request, model_admin):
        return (
            ("0", "Zero Length Bombcasts"),
        )

    def queryset(self, request, queryset):
        if self.value() == "0":
            return queryset.filter(duration__exact=0)
        
        return queryset

class BombcastsAdmin(admin.ModelAdmin):
    list_filter = (BombcastsFilters,)

admin.site.register(Bombcasts, BombcastsAdmin)
admin.site.register(RadioPlaylist)