﻿from apps.api.models import RadioPlaylist
from bombcastradio import config

from django.http import HttpResponse

def get_radio_playlist(request, count = config.RADIO_PLAYLIST_API_DEFAULT_NUM_TO_DISPLAY):
    count = int(count)

    json_playlist = RadioPlaylist.objects.serialize_playlist(count)
    return HttpResponse(json_playlist, content_type='application/json')