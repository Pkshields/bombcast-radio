﻿from bombcastradio import config
from utils import bombcast_feed, datetime_helper
from apps.api.models import Bombcasts, RadioPlaylist

from django.http import HttpResponse
from datetime import datetime

def update_db(request):
    parsed_bombcast_feed = bombcast_feed.parse_bombcast_feed()
    parsed_bombcast_feed_length = len(parsed_bombcast_feed)
    last_bombcast_added = Bombcasts.objects.get_last_bombcast()
    
    if last_bombcast_added is not None:
        for i in range(parsed_bombcast_feed_length - 1, -1, -1):
            if parsed_bombcast_feed[i]["title"] == last_bombcast_added.title:
                parsed_bombcast_feed = parsed_bombcast_feed[i+1 : parsed_bombcast_feed_length]
                break

    parsed_bombcast_feed_length = len(parsed_bombcast_feed)

    return_string = ""
    for bombcast in parsed_bombcast_feed:
        return_string += "Added Bombcast \"" + bombcast["title"] + "\" \n"
    return_string += "Done!"

    Bombcasts.objects.add_to_database(parsed_bombcast_feed)

    return HttpResponse(return_string, content_type='text/plain')


def update_radio_playlist(request):
    return_string = "Cleaned played bombcasts and added: \n"

    # 1: Clean up current playlist table
    RadioPlaylist.objects.clean_played_bombcasts()

    # 2: Add new bombcasts
    last_bombcast_in_current_playlist = RadioPlaylist.objects.get_last_bombcast()
    if last_bombcast_in_current_playlist is not None:
        time_to_start = last_bombcast_in_current_playlist.start_time
        time_to_start = datetime_helper.add_secs(time_to_start, last_bombcast_in_current_playlist.bombcast.duration)

        time_left_to_play = time_to_start - datetime_helper.utcnow()
        seconds_currently_generated = time_left_to_play.total_seconds();

    else:
        time_to_start = datetime_helper.utcnow()
        seconds_currently_generated = 0

    random_bombcasts = Bombcasts.objects.get_bombcasts_in_random_order()

    radio_playlist = []

    for bombcast in random_bombcasts:
        if (seconds_currently_generated >= config.RADIO_PLAYLIST_SECONDS_TO_GENERATE):
            break

        if bombcast.duration > 0:
            radio_data = { "bombcast": bombcast,
                           "start_time": time_to_start }
            radio_playlist.append(radio_data)

            return_string += bombcast.title + "\n"
            time_to_start = datetime_helper.add_secs(time_to_start, bombcast.duration)

            seconds_currently_generated += bombcast.duration

    RadioPlaylist.objects.add_to_database(radio_playlist)

    return HttpResponse(return_string, content_type='text/plain')