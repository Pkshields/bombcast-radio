from apps.api.views import update
from utils import datetime_helper
from utils.datetime_helper import WeekdayEnum

from django.core.management.base import BaseCommand
from django.http import HttpRequest

class Command(BaseCommand):
     
    def handle(self, *args, **options):
        http_request = update.update_db(HttpRequest())
        print(http_request.content.decode("utf-8"))