﻿from apps.api.managers.bombcasts_manager import BombcastsManager
from apps.api.managers.radio_playlist_manager import RadioPlaylistManager
from utils import datetime_helper

from django.db import models

class Bombcasts(models.Model):
    title = models.CharField(max_length = 60)
    description = models.TextField()
    url = models.CharField(max_length = 120)
    img_url = models.CharField(max_length = 100)
    pub_date = models.DateTimeField()
    duration = models.IntegerField()

    objects = BombcastsManager()

    # Human Readable names in Django Admin
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name_plural = "Bombcasts"


class RadioPlaylist(models.Model):
    bombcast = models.ForeignKey("Bombcasts")
    start_time = models.DateTimeField(primary_key=True)

    objects = RadioPlaylistManager()

    # Methods
    def end_time(self):
        return datetime_helper.add_secs(self.start_time, self.bombcast.duration)

    # Human Readable names in Django Admin
    def __str__(self):
        return datetime_helper.to_string(self.start_time) + ": " + self.bombcast.title
    
    class Meta:
        verbose_name_plural = "Radio Playlist"
        ordering = ["start_time"]