# Bombcast Radio
 
Bombcast Radio is a live 24/7 stream of the Giant Bombcast! Join thousands listening to current and classic Bombcast.

This is both the website front-end and API used to feed the website and apps with the Bombcast Radio data.

## Deployment

This repo contains a modified code set from the last version that was deployed to a server which removes any personal credentials or IDs that were hardcoded into the app. A [Vagrant](https://www.vagrantup.com/) script is supplied to run the webapp locally, but this webapp **should not be run in production again**. It was built on a long outdated version of Django and other dependencies and it would require a lot of work and testing to get production ready again.

### Run Locally

First, install [Vagrant](https://www.vagrantup.com/docs/installation/). Then, navigate to the root of this source directory and run...

```vagrant up```

Once the VM has started successfully, the app will be available at `http://192.168.33.10/`.

### Notes

The app is built to run with a MySQL database, but for the purposes of the Vagrant demo the app will be running against a local SQLite DB.